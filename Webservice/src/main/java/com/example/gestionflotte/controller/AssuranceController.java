package com.example.gestionflotte.controller;
import com.example.gestionflotte.exception.ErrorException;
import com.example.gestionflotte.service.Error;
import com.example.gestionflotte.service.*;

import com.example.gestionflotte.exception.ResourceNotFoundException;
import com.example.gestionflotte.model.*;
import com.example.gestionflotte.repository.AssuranceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;


@CrossOrigin("*")
@RestController
@RequestMapping("/assurances")

public class AssuranceController {

    @Autowired
    private AssuranceRepository repo;

    @GetMapping
    public Data getAllAssurance() {
        return new Data(repo.findAll());
    }
    @GetMapping("{mois}")
    public Object getAssurance(@PathVariable String mois){
        try{
            int month=Integer.parseInt(mois);
            if(month==3)
                return new Data(repo.InThreeMonth());
            if(month==1)
                return new Data(repo.InOneMonth());
            return new Error(null);
        }catch(Exception e){
            return new Error( (e));
        }
    }
}
