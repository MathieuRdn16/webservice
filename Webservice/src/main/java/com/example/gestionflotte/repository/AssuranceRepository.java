package com.example.gestionflotte.repository;

import com.example.gestionflotte.model.Assurance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface AssuranceRepository extends JpaRepository<Assurance, String> {
    @Query(value = "select * from v_assuranceUn",nativeQuery = true)
    List<Assurance> InOneMonth();
    @Query(value = "select * from v_assuranceTrois",nativeQuery = true)
    List<Assurance> InThreeMonth();
}
