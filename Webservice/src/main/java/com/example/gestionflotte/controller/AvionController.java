package com.example.gestionflotte.controller;

import com.example.gestionflotte.exception.ResourceNotFoundException;
import com.example.gestionflotte.model.Avion;
import com.example.gestionflotte.repository.AvionRepository;
import com.example.gestionflotte.service.Data;
import com.example.gestionflotte.service.DeleteAvionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/avions")

public class AvionController {

    @Autowired
    private AvionRepository avionRepository;
    @Autowired
    private DeleteAvionService deleteservice;

    @GetMapping
    public Object getAllAvions(){
        try{
        return new Data( avionRepository.findAll());
        }catch(Exception e){
            return new Error(e);
        }
    }
    // build create employee REST API
    @PostMapping
    public Object createAvion(@RequestBody Avion avion) {
        try{
            avionRepository.changeProfil(avion.getProfil(), avion.getIdavion());
            return new Data(1);
        }catch(Exception e){
            return new Error(e);
        }
    }
    // build get employee by id REST API
    @GetMapping("{id}")
    public Object getAvionById(@PathVariable  int id){
        try {
            Avion avion = avionRepository.findById("1").get();
            if(avion==null) {
                return new Error(new ResourceNotFoundException("Avion not exist with id:" + id));
            }
            return new Data(ResponseEntity.ok(avion));
        }catch (Exception e){
            return new Error(e);
        }

    }

    @PutMapping("{id}")
    public Object updateAvion(@PathVariable String id,@RequestBody Avion avion) {
        try{
            Avion updateAvion = avionRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Avion not exist with id: " + id));

            updateAvion.setNom(avion.getNom());
            updateAvion.setNumero(avion.getNumero());

            avionRepository.save(updateAvion);

            return new Data(ResponseEntity.ok(updateAvion));
        }catch(Exception e){
            return new Error (e);
        }
    }
    @DeleteMapping("{id}")
    public Object deleteAvion(@PathVariable String id) {
        try{
            Avion deleteAvion = avionRepository.findById(id).get();
            if(deleteAvion==null)
                return new Error(new ResourceNotFoundException("Avion not exist with id: "+id));
            deleteservice.save(deleteAvion);
            return new Data(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        }catch(Exception e){return new Error(e);}

    }
}
