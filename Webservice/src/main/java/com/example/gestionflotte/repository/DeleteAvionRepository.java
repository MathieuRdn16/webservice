package com.example.gestionflotte.repository;

import com.example.gestionflotte.model.DeleteAvion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeleteAvionRepository extends JpaRepository<DeleteAvion, String> {

}
