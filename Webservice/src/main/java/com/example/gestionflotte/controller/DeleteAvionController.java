package com.example.gestionflotte.controller;

import com.example.gestionflotte.exception.*;
import com.example.gestionflotte.model.DeleteAvion;
import com.example.gestionflotte.service.*;
import com.example.gestionflotte.service.Error;
import com.example.gestionflotte.repository.DeleteAvionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@CrossOrigin("*")
@RestController
@RequestMapping("/deleteAvions")

public class DeleteAvionController {
    @Autowired
    private DeleteAvionRepository DeleteAvionRepository;

    @GetMapping
    public Object getAllDeleteAvions(){
        return new Data(DeleteAvionRepository.findAll());
    }
    // build create employee REST API
    @PostMapping
    public Object createDeleteAvion(@RequestBody DeleteAvion DeleteAvion) {
        try{
            return new Data(DeleteAvionRepository.save(DeleteAvion));
        }catch(Exception e){
            return new Error(e);
        }
    }
    // build get employee by id REST API
    @GetMapping("{id}")
    public Object getDeleteAvionById(@PathVariable  String id){
        try{
            DeleteAvion DeleteAvion = DeleteAvionRepository.findById(id).get();
            if(DeleteAvion==null)
                return new Error(new ResourceNotFoundException("Voiture not exist with id:" + id));
            return new Data(ResponseEntity.ok(DeleteAvion));
        }catch(Exception e){
            return new Error(e);
        }

    }

    @PutMapping("{id}")
    public Object updateDeleteAvion(@PathVariable String id,@RequestBody DeleteAvion employeeDetails) {
        try{
            DeleteAvion updateDeleteAvion = DeleteAvionRepository.findById(id).get();
            if(updateDeleteAvion==null)
                return new Error(new ResourceNotFoundException("Voiture not exist with id: " + id));
        DeleteAvionRepository.save(updateDeleteAvion);

        return new Data(ResponseEntity.ok(updateDeleteAvion));
        }catch(Exception e){
            return new Error(e);
        }
    }
    @DeleteMapping("{id}")
    public Object deleteDeleteAvion(@PathVariable String id){
        try{
            DeleteAvion DeleteAvion = DeleteAvionRepository.findById(id).get();
            if(DeleteAvion==null){
                return new Error (new ResourceNotFoundException("Voiture not exist with id: " + id));
            }

            DeleteAvionRepository.delete(DeleteAvion);

            return new Data(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        }catch(Exception e){
            return new Error(e);
        }
    }
}
