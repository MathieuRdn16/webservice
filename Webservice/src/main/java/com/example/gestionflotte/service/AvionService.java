package com.example.gestionflotte.service;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.gestionflotte.repository.*;
import com.example.gestionflotte.model.*;
import org.springframework.stereotype.Service;

import java.util.*;
@Service
public class AvionService {

    @Autowired
    private DeplacementRepository repo;
    public List<Deplacement> findAll(){
        return repo.findAll();
    }
}
