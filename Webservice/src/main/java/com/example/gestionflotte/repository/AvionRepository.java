package com.example.gestionflotte.repository;

import com.example.gestionflotte.model.Assurance;
import com.example.gestionflotte.model.Avion;
import com.example.gestionflotte.model.Deplacement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AvionRepository extends JpaRepository<Avion, String> {
    @Query(value = "update avion set profil=:profil where  idavion=:id",nativeQuery = true)
    void changeProfil(byte[] profil ,int id);
}

