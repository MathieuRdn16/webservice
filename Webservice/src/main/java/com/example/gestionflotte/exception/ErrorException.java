package com.example.gestionflotte.exception;

public class ErrorException extends Exception {
    public ErrorException(String message){
        super(message);
    }
}
