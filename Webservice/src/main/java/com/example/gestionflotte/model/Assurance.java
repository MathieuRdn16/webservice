package com.example.gestionflotte.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "assurance")
public class Assurance {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idassurance;

    @Column(name = "payement")
    private Date payement;

    @Column(name = "expiration")
    private Date expiration;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="idavion")
    private Avion avion;
}

