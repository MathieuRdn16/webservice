package com.example.gestionflotte.service;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.gestionflotte.repository.DeleteAvionRepository;
import com.example.gestionflotte.model.*;
import org.springframework.stereotype.Service;
@Service
public class DeleteAvionService {
    @Autowired
    private DeleteAvionRepository deleterepo;
    public DeleteAvion save(Avion vehicule){
        DeleteAvion dv=new DeleteAvion(vehicule.getIdavion());
        return deleterepo.save(dv);
    }
}
