package com.example.gestionflotte.model;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tokenage")

public class TokenAge {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idtokenage;
    @Column(name="valeur")
    private int valeur;
}
