package com.example.gestionflotte.model;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.sql.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "deplacement")

public class Deplacement {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int iddeplacement;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="idavion")
    private Avion avion;

    @Column(name = "datedep")
    private Date datedep;

    @Column(name = "debutkm")
    private Integer debutkm;

    @Column(name = "finkm")
    private Integer finkm;

}
