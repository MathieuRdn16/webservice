package com.example.gestionflotte.controller;
import com.example.gestionflotte.exception.ErrorException;
import com.example.gestionflotte.service.Error;
import com.example.gestionflotte.service.*;

import com.example.gestionflotte.exception.ResourceNotFoundException;
import com.example.gestionflotte.model.*;
import com.example.gestionflotte.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;


@CrossOrigin("*")
@RestController
@RequestMapping("/admins")

public class AdminController {

    @Autowired
    private AdminRepository AdminRepository;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private TokenAgeService ageService;
    @GetMapping
    public Data getAllAdmins() {
        return new Data(AdminRepository.findAll());
    }

    @PostMapping
    public Object login(@RequestBody Admin admin) {
        try{
            int id=0;
            id=Admin.auth(admin,AdminRepository.findAll());
            if(id==0)
                return new Error( new ErrorException("Authentification incorrect"));
            admin.setIdadmin(id);
            String token=Token.generateToken(admin.getNom()+admin.getMdp());
            Token tk=new Token(token,admin.getIdadmin(),ageService.expiration());
            return new Data(tokenService.save(tk));
        }catch(Exception e){
            return new Error(e);
        }

    }

    @GetMapping("{id}")
    public Object getAdminById(@PathVariable String id){
        try{
            Admin admin = AdminRepository.findById(id).get();
            return new Data(ResponseEntity.ok(admin));
        }catch(Exception e){
            return new Error( (new ResourceNotFoundException("not exist with id: " + id)));
        }
    }

    @PutMapping("{id}")
    public Object updateAdmin(@PathVariable String id, @RequestBody Admin AdminDetails) {
        try{
            Admin updateAdmin = AdminRepository.findById(id).get();
                if(updateAdmin==null)
                return new Error( new ResourceNotFoundException("not exist with id: " + id));
            updateAdmin.setIdadmin(AdminDetails.getIdadmin());
            updateAdmin.setNom(AdminDetails.getNom());
            updateAdmin.setMdp(AdminDetails.getMdp());
            AdminRepository.save(updateAdmin);
            return new Data(ResponseEntity.ok(updateAdmin));
        }catch(Exception e){
            return new Error(e);
        }
    }

    @DeleteMapping("{id}")
    public Object deleteAdmin(@PathVariable String id) {
        try{
            Admin Admin = AdminRepository.findById(id).get();
            if(Admin==null)
                return new Error(new ResourceNotFoundException("not exist with id: " + id));

            AdminRepository.delete(Admin);
            return new Data(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        }catch(Exception e){
            return new Error(e);
        }


    }
}
