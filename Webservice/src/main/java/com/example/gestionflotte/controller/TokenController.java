package com.example.gestionflotte.controller;

import com.example.gestionflotte.exception.ResourceNotFoundException;
import com.example.gestionflotte.model.Token;
import com.example.gestionflotte.repository.TokenRepository;
import com.example.gestionflotte.service.*;
import com.example.gestionflotte.service.Error;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/tokens")

public class TokenController {

    @Autowired
    private TokenRepository tokenRepository;
    @Autowired
    private TokenService tokenserv;

    @GetMapping
    public Object getAllTokens(){
        try{
        return new Data(tokenRepository.findAll());
        }catch(Exception e){
            return new Error(e);
        }
    }
    @PostMapping
    public Object createToken(@RequestBody Token token) {
        try{
            return new Data (tokenserv.verification(token.getValeur(),tokenRepository.findAll()));
        }catch(Exception e){
            return new Error(e);
        }
    }
    @GetMapping("{id}")
    public Object getTokenById(@PathVariable  String id){
        try{
            Token token = tokenRepository.findById(id).get();
            if(token==null)
                return new Error(new ResourceNotFoundException("Token not exist with id:" + id));
            return new  Data(ResponseEntity.ok(token));
        }catch(Exception e){
            return new Error(e);
        }

    }

    @PutMapping("{id}")
    public Object updateToken(@PathVariable String id,@RequestBody Token token) {
        try{
            Token updateToken = tokenRepository.findById(id).get();
            if(updateToken==null)
                return new Error(new ResourceNotFoundException("Token not exist with id: " + id));

            updateToken.setValeur(token.getValeur());
            updateToken.setIdadmin(token.getIdadmin());
            updateToken.setDateexpiration(token.getDateexpiration());

            tokenRepository.save(updateToken);

            return new Data(ResponseEntity.ok(updateToken));
        }catch (Exception e){
            return new Error(e);
        }

    }
    @DeleteMapping("{id}")
    public Object deleteToken(@PathVariable String id) {
        try{
            Token deleteToken = tokenRepository.findById(id).get();
            if(deleteToken==null)
                return new Error( new ResourceNotFoundException("token not exist with id: "+id));
            tokenRepository.delete(deleteToken);
            return new Data(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        }catch(Exception e){
            return new Error(e);
        }

    }
}
