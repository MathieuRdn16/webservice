package com.example.gestionflotte.repository;

import com.example.gestionflotte.model.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface TokenRepository extends JpaRepository<Token, String> {
    // all crud database methods
}
