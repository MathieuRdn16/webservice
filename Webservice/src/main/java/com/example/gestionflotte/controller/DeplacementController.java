package com.example.gestionflotte.controller;
import com.example.gestionflotte.exception.ResourceNotFoundException;
import com.example.gestionflotte.model.Deplacement;
import com.example.gestionflotte.service.Error;
import com.example.gestionflotte.service.*;
import com.example.gestionflotte.repository.DeplacementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/deplacements")
public class DeplacementController {
    @Autowired
    private DeplacementRepository DeplacementRepository;
    private AvionService AvionS;

    @GetMapping
    public Object getAllDeplacement(){
        try{
            return new Data( DeplacementRepository.findAll());
        }catch(Exception e){
            return new Error(e);
        }
    }

    @PostMapping
    public Object createEmployee(@RequestBody Deplacement deplacement) {
        try{
            return new Data(DeplacementRepository.save(deplacement));
        }catch(Exception e){
            return new Error(e);
        }
    }
    @GetMapping("{idavion}")
    public Object getDeplacementById(@PathVariable  int idavion){
        try{
            return new Data(DeplacementRepository.deplByAvion(idavion));
        }catch(Exception e){
            return new Error(e);
        }

    }
    @PutMapping("{iddeplacement}")
    public Object updateDeplacement(@PathVariable String iddeplacement,@RequestBody Deplacement DDetails) {
        try{
            Deplacement updateDeplacement = DeplacementRepository.findById(iddeplacement).get();
            if(updateDeplacement==null)
                    return new Error(new ResourceNotFoundException("Deplacement not exist with id: " + iddeplacement));

            updateDeplacement.setDebutkm(DDetails.getDebutkm());
            updateDeplacement.setFinkm(DDetails.getFinkm());

            DeplacementRepository.save(updateDeplacement);

            return new Data(ResponseEntity.ok(updateDeplacement));
        }catch(Exception e){
            return new Error(e);
        }

    }
    @DeleteMapping("{id}")
    public Object deleteVehicule(@PathVariable String id) {
        try {
            Deplacement deleteDeplacement = DeplacementRepository.findById(id).get();
            if(deleteDeplacement ==null)
                return new Error( new ResourceNotFoundException("Deplacement not exist with id: "+id));
            DeplacementRepository.delete(deleteDeplacement);
            return new Data(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        }catch(Exception e){
            return new Error(e);
        }
    }
}
