package com.example.gestionflotte.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Base64;

@Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @Entity
    @Table(name = "avion")
    public class Avion {

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int idavion;

        @Column(name = "nom")
        private String nom;

        @Column(name = "numero")
        private String numero;
        @Column(name = "profil")
        private byte[] profil;
        @ManyToOne(cascade = CascadeType.MERGE)
        @JoinColumn(name="idcategorie")
        private Categorie categorie;
    }

