package com.example.gestionflotte.service;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.gestionflotte.repository.*;
import com.example.gestionflotte.model.*;
import org.springframework.stereotype.Service;
import java.util.*;
@Service
public class AdminService {

    @Autowired
    private AdminRepository repo;
    public List<Admin> findAll(){
        return repo.findAll();
    }
    public Admin findById(String id){
        Admin ad=repo.findById(id).get();
        return ad;
    }
}
