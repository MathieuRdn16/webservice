package com.example.gestionflotte.repository;

import com.example.gestionflotte.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DeplacementRepository extends JpaRepository<Deplacement, String> {
    @Query(value = "select * from deplacement where idavion=:id",nativeQuery = true)
    List<Deplacement> deplByAvion(int id);
}
