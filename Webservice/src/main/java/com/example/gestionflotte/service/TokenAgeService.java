package com.example.gestionflotte.service;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.gestionflotte.repository.TokenAgeRepository;
import com.example.gestionflotte.model.*;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

@Service
public class TokenAgeService {
    @Autowired
    private TokenAgeRepository tokenAgeRepo;
     public List<TokenAge> findAll(){
        return tokenAgeRepo.findAll();
     }
     public TokenAge getOne(){
         return this.findAll().get(0);
     }
     public  Timestamp expiration(){
         LocalDateTime now=LocalDateTime.now();
         Timestamp delai=Timestamp.valueOf(now);
         TokenAge age=this.getOne();
         delai.setTime(delai.getTime() + TimeUnit.MINUTES.toMillis(age.getValeur()));
         return delai;
     }
}
