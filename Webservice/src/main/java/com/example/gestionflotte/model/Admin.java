package com.example.gestionflotte.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.*;
import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "administrateur")
public class Admin {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idadmin;

    @Column(name = "nom")
    private String nom;

    @Column(name = "mdp")
    private String mdp;
    public static int auth(Admin admin, List<Admin> list){
        for(int r=0;r<list.size();r++){
            Admin comp=(Admin)list.get(r);
            if(comp.getNom().compareTo(admin.getNom())==0&&comp.getMdp().compareTo(admin.getMdp())==0)
                return comp.getIdadmin();
        }
        return 0;
    }
}
