package com.example.gestionflotte.repository;

import com.example.gestionflotte.model.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface AdminRepository extends JpaRepository<Admin, String> {
    // all crud database methods
}
