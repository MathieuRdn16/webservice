package com.example.gestionflotte.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "entretien")
public class Entretien {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int identretien;

    @Column(name = "dateentretien")
    private Date dateentretien;

    @Column(name = "prochainent")
    private Date prochaineent;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="idavion")
    private Avion avion;
}

