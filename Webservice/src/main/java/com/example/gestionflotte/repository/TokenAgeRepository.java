package com.example.gestionflotte.repository;

import com.example.gestionflotte.model.TokenAge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface TokenAgeRepository extends JpaRepository<TokenAge, String> {
    // all crud database methods
}
